const Tarea = require("./tarea");
const Role = require("./role");
const Server = require("./server");
const Usuario = require("./usuario");

module.exports = {
  Tarea,
  Role,
  Server,
  Usuario,
};
