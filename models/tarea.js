const { Schema, model } = require("mongoose");

const TareaSchema = Schema({
  nameTarea: {
    type: String,
    required: [true, "El nombre es obligatorio"],
    // unique: true,
  },
  completado: {
    type: Boolean,
    default: false,
    required: true,
  },
  state: {
    type: Boolean,
    default: true,
    required: true,
  },
  usuario: {
    //para saber que usuario creo la tarea
    type: Schema.Types.ObjectId,
    ref: "Usuario",
    required: true,
  },
});

TareaSchema.methods.toJSON = function () {
  const { __v, state, ...data } = this.toObject();
  return data;
};

module.exports = model("Tarea", TareaSchema);
