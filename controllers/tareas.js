const { response } = require("express");
const { Tarea } = require("../models");

const obtenerTareas = async (req, res = response) => {
  const { limite = 5, desde = 0 } = req.query;
  const query = { state: true };

  const [total, tareas] = await Promise.all([
    Tarea.countDocuments(query),
    Tarea.find(query)
      .populate("usuario", "names")
      .skip(Number(desde))
      .limit(Number(limite)),
  ]);

  res.json({
    total,
    tareas,
  });
};

const obtenerTarea = async (req, res = response) => {
  const { id } = req.params;
  const tarea = await Tarea.findById(id).populate("usuario", "names");

  res.json(tarea);
};

const crearTarea = async (req, res = response) => {
  const nameTarea = req.body.nameTarea.toUpperCase();

  // const tareaDB = await Tarea.findOne({ nameTarea });

  // if (tareaDB) {
  //   return res.status(400).json({
  //     status: 0,
  //     msg: `La tarea ${tareaDB.nameTarea}, ya existe`,
  //   });
  // }

  // Generar la data a guardar
  const data = {
    nameTarea,
    usuario: req.usuario._id,
  };

  const tarea = new Tarea(data);

  // Guardar DB
  await tarea.save();

  res.status(201).json({ status: 1, tarea }); // SI SE CREA ALGo se manda el 201
};

const actualizarTarea = async (req, res = response) => {
  const { id } = req.params;
  const { state, usuario, ...data } = req.body;

  data.nameTarea = data.nameTarea.toUpperCase();
  data.usuario = req.usuario._id;

  ////////

  // const nameTarea = req.body.nameTarea.toUpperCase();

  // const tareaDB = await Tarea.findOne({ nameTarea });

  // if (tareaDB) {
  //   return res.status(400).json({
  //     status: 0,
  //     msg: `La tarea ${tareaDB.nameTarea}, ya existe`,
  //   });
  // }
  ////////
  const tarea = await Tarea.findByIdAndUpdate(id, data, { new: true });

  res.json({ status: 1, tarea });
};

const borrarTarea = async (req, res = response) => {
  const { id } = req.params;
  const tareaBorrada = await Tarea.findByIdAndUpdate(
    id,
    { state: false },
    { new: true }
  );

  res.json({ status: 1, tareaBorrada });
};

module.exports = {
  crearTarea,
  obtenerTareas,
  obtenerTarea,
  actualizarTarea,
  borrarTarea,
};
