const { Router } = require("express");
const { check } = require("express-validator");

const {
  validarJWT,
  validarCampos,
  esAdminRole,
  tieneRole,
} = require("../middlewares");
const { esRoleValido } = require("../helpers/db-validators");
const {
  crearTarea,
  obtenerTareas,
  obtenerTarea,
  actualizarTarea,
  borrarTarea,
} = require("../controllers/tareas");
const { existeTareaPorId } = require("../helpers/db-validators");

const router = Router();

/**
 * {{url}}/api/tarea
 */

//  Obtener todas las Tarea - publico
router.get("/", obtenerTareas);

// Obtener una Tarea por id - publico
router.get(
  "/:id",
  [
    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeTareaPorId),
    validarCampos,
  ],
  obtenerTarea
);

// Crear Tarea - privado - cualquier persona con un token válido
router.post(
  "/",
  [
    validarJWT,
    tieneRole("ADMIN_ROLE", "USER_ROLE"), //SON LOS ROLES QUE PODRAN modificar una Tarea
    check("nameTarea", "El nombre es obligatorio").not().isEmpty(),
    validarCampos,
  ],
  crearTarea
);

// Actualizar - privado - cualquiera con token válido
router.put(
  "/:id",
  [
    validarJWT,
    tieneRole("ADMIN_ROLE", "USER_ROLE"), //SON LOS ROLES QUE PODRAN modificar una Tarea

    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeTareaPorId),
    check("nameTarea", "El nombre es obligatorio").not().isEmpty(),
    validarCampos,
  ],
  actualizarTarea
);

// Borrar una Tarea - Admin
router.delete(
  "/:id",
  [
    validarJWT,
    // esAdminRole,
    tieneRole("ADMIN_ROLE", "USER_ROLE"), //SON LOS ROLES QUE PODRAN modificar una Tarea

    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeTareaPorId),
    validarCampos,
  ],
  borrarTarea
);

module.exports = router;
